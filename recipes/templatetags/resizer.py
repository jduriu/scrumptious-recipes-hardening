from django import template

register = template.Library()


def resize_to(ingredient, target):
    servings = ingredient.recipe.servings

    if servings and target:
        try:
            ratio = int(target) / servings
            return ratio * ingredient.amount
        except Exception:
            pass

    return ingredient.amount


register.filter(resize_to)
